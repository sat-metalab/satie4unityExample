// Satie4Unity, audio rendering support for Unity
// Copyright (C) 2016  Zack Settel

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// -----------------------------------------------------------

This repository has a submodule called satie4Unity, contianing required scripts. When cloning this repo, make sure to use the --recursive flag.  
Thus, to clone this repo, you need to do the following.

>git clone --recursive THIS_REPO

# checkout this branch for a simplified version of the Unity Project, otherwise checkout the master or develop branch

git submodule update --init --recursive




Satie4Unity provides SuperCollider-based audio rendering support for Unity3D. A Unity3D package, containing a demo scene and all the necessary resources is provided, as well as the Unity3D scripts (already in the package) themselves.

To run the example, create an empty scene in Unity, then import the package. Then launch supercollider and evaluate the file "satieOSCexample.scd”.  Then run the scene.


The distribution contains the following items:


README.txt

control :  contains TouchOSC control skins and pure data patches for scene control

satie4unityProject  : the Unity Project

satie4unityScripts  : scripts for automating launch

satieProject  :  the Satie Project

tests  :  various diagostics and tests  (supercollider and puredata)




Running the Project:

To use Satie in power user mode:

From the Terminal
cd satie4unityExample/satieProject
sh  bootProject.sh    -f <listenerFormat, e.g.   domeVBAP>


Launch the Unity Project, and recall the scene "SatieTest"  or "SimpleExample"
Verify sure that the gameRoot satieSetup component has the proper IP address for the machine running the satie process
Run the scene.





To use Satie in "Turnkey Mode"


Using Audio MIDI Setup, create an aggregate device, with soundflower64 followed by Madi32

Set the output audio driver of all DAW's sending audio (chan 1 - 64) to satie, to "soundFlower64".


From the Terminal
cd ~/Library/Application\ Support/SuperCollider/downloaded-quarks/SATIE-quark/server
sh satie -i 64   -n  -m -t -v -O 64 -d "Aggregate Device"

Launch the Unity Project, and recall the scene "SatieTurnKeyExample"
Verify sure that the gameRoot satieSetup component has the proper IP address for the machine running the satie process
Run the scene.













Supercollider Requirements:

Supercollider 3.9 (with supernova)
with the sc3Plugins (with supernova)
and with the following Quarks:

    Ambiem
    Ctk
    MathLib
    Satie-quark atk-sc3


To install quarks, you can evaluate the following in the supercollider IDE

    Quarks.install("AmbIEM")
    Quarks.install(“Ctk”)
    Quarks.install(“MathLib”)
    Quarks.install("https://github.com/ambisonictoolkit/atk-sc3.git");



If the installed Quarks are fogotton, read on:

Note that in Supercollider, sometimes the file:   Platform.userConfigDir +/+ "sclang_conf.yaml"  us undefined..


// check to see if this exists
Platform.userConfigDir +/+ "sclang_conf.yaml"


// if not defined, install the Quarks, then verify and store, as shown below


LanguageConfig.includePaths
LanguageConfig.store




    




