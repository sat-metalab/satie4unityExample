﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class toggleText : MonoBehaviour {

    private Text myText;

    // Use this for initialization
    void Start () 
    {
        myText = transform.GetComponent(typeof(Text)) as Text;


        if (myText == null)
            myText = gameObject.GetComponentInChildren<Text> (true);    // else try my children
    }

    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (myText != null)
                myText.enabled = !myText.enabled;
        }
    }
}
