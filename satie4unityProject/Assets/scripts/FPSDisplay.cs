﻿using UnityEngine;
using System.Collections;

public class FPSDisplay : MonoBehaviour
{
    float deltaTime = 0.0f;
    public float size = 5f;
//	private float scaler = 2 / 100; // Never used warning
    private bool _start = false;
    public bool _display = false;


    void Start()
    {
        _start = true;
    }

    void Update()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
    }

    public void FPSdisplay(bool state)
    {
        _display = state;
    }

    public void FPSdisplay(float state)
    {
        _display = (state < 1) ? false:true;
    }


    void OnGUI()
    {
        if (!_start || !_display)
            return;
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(0, 0, w, size*h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = (int)(size * h) * 2 / 100;
        style.normal.textColor = Color.yellow;
        float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;
        string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
        GUI.Label(rect, text, style);
    }
}